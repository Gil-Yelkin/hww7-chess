/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/

#pragma warning(disable : 4996)

#include "Pipe.h"
#include <iostream>
#include <thread>
#include "../Backend/Board.h"

using std::cout;
using std::endl;
using std::string;
#define BUFFER_SIZE 1024


void main()
{
	srand(time_t(NULL));

	Pipe p;
	bool isConnect = p.connect();
	Board board;
	
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	

	char msgToGraphics[BUFFER_SIZE];
	// msgToGraphics should contain the board string accord the protocol
	// YOUR CODE

	strcpy_s(msgToGraphics, INITIAL_BOARD_STR); // just example...
	
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		// return result to graphics
		itoa(board.makeMove(msgFromGraphics), msgToGraphics, BUFFER_SIZE);
		p.sendMessageToGraphics(msgToGraphics);

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();	
}
