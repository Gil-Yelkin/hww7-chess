#pragma once
#include "Piece.h"

class Bishop : public Piece
{
public:
	Bishop(const ubyte& x, const ubyte& y, const Color& color, Board& board);
	virtual const Type getType() const override;
	virtual void validateMove(const ubyte& x, const ubyte& y) const override;
	static void validateBishop(ubyte xCurr, ubyte yCurr, const ubyte& xDst, const ubyte& yDst, Board& board);
};
