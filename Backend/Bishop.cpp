#include "Bishop.h"

Bishop::Bishop(const ubyte& x, const ubyte& y, const Color& color, Board& board)
	: Piece(x, y, color, board) 
{}

const Type Bishop::getType() const { return BISHOP; }

void Bishop::validateMove(const ubyte& x, const ubyte& y) const
{
	Piece::validateMove(x, y);

	// Check if not going diagonally
	if (abs(x - this->x) != abs(y - this->y))
		throw invalidMove_IllegalMove;

	validateBishop(this->x, this->y, x, y, this->board);
}

void Bishop::validateBishop(ubyte xCurr, ubyte yCurr, const ubyte& xDst, const ubyte& yDst, Board& board)
{
	// Check if path is not clear
	const bool directionX = (xDst - xCurr > 0);
	const bool directionY = (yDst - yCurr > 0);

	// Exclude current cell, then update x & y to check according to the direction
validateBishopLoop:
	directionX ? ++xCurr : --xCurr;
	directionY ? ++yCurr : --yCurr;

	if (directionX && directionY && xCurr >= xDst)
		return;
	else if (directionX && !directionY && xCurr >= xDst)
		return;
	else if (!directionX && directionY && xCurr <= xDst)
		return;
	else if (!directionX && !directionY && xCurr <= xDst)
		return;

	// Check if current cell is not empty
	if (board(xCurr, yCurr))
		throw invalidMove_IllegalMove;
	goto validateBishopLoop;
}
