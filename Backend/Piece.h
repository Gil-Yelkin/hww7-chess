#pragma once
#include <iostream>
#include "Utils.h"
#include "Board.h"

class Board;

class Piece
{
protected:
	// fields
	ubyte x, y;
	Color color;
	Board& board;

	// methods
	virtual void validateMove(const ubyte& x, const ubyte& y) const;

public:
	Piece(const ubyte& x, const ubyte& y, const Color& color, Board& board);
	
	static Piece* nullPiece;

	// getters
	const Color& getColor() const;
	virtual const Type getType() const = 0;
	const ubyte getX() const;
	const ubyte getY() const;
	void move(const ubyte& x, const ubyte& y);
};
