#include "Board.h"
#include "Bishop.h"
#include "Rook.h"
#include "Pawn.h"
#include "Knight.h"
#include "king.h"
#include "Queen.h"

Board::Board() : blackKing(*(new King(4, 0, Black, *this))), whiteKing(*(new King(4, 7, White, *this)))
{
	this->turn = White;

	// initializing each piece in it's starting position
	(*this)(0, 0) = new Rook(0, 0, Black, *this);
	(*this)(1, 0) = new Knight(1, 0, Black, *this);
	(*this)(2, 0) = new Bishop(2, 0, Black, *this);
	(*this)(3, 0) = new Queen(3, 0, Black, *this);
	(*this)(4, 0) = &(this->blackKing);
	(*this)(5, 0) = new Bishop(5, 0, Black, *this);
	(*this)(6, 0) = new Knight(6, 0, Black, *this);
	(*this)(7, 0) = new Rook(7, 0, Black, *this);

	for (ubyte col = 0; col < ROWS; col++)
	{
		(*this)(col, 1) = new Pawn(col, 1, Black, *this);
		(*this)(col, 2) = nullptr;
		(*this)(col, 3) = nullptr;
		(*this)(col, 4) = nullptr;
		(*this)(col, 5) = nullptr;
		(*this)(col, 6) = new Pawn(col, 6, White, *this);
	}

	(*this)(0, 7) = new Rook(0, 7, White, *this);
	(*this)(1, 7) = new Knight(1, 7, White, *this);
	(*this)(2, 7) = new Bishop(2, 7, White, *this);
	(*this)(3, 7) = new Queen(3, 7, White, *this);
	(*this)(4, 7) = &(this->whiteKing);
	(*this)(5, 7) = new Bishop(5, 7, White, *this);
	(*this)(6, 7) = new Knight(6, 7, White, *this);
	(*this)(7, 7) = new Rook(7, 7, White, *this);
}

Board::~Board()
{
	ubyte col = 0, row = 0;

	for (col = 0; col < COLS; col++)
	{
		for (row = 0; row < ROWS; row++)
		{
			delete (*this)(col, row);
		}
	}
}

Piece*& Board::operator()(const ubyte& x, const ubyte& y, const bool& doThrow)
{
	if (x >= COLS || y >= ROWS)
	{
		if (doThrow)
			throw invalidMove_InvalidCellIndex;
		else
			return Piece::nullPiece; // Shiluv shel Shonny ve Yarden
	}

	return this->pieces[x][y];
}

Piece*& Board::operator()(const coords& pos, const bool& doThrow)
{
	return (*this)(pos.x, pos.y);
}

const Color Board::getTurn() const
{
	return this->turn;
}

void Board::updateTurn()
{
	this->turn = (Color)!(bool)this->turn;
}

const bool Board::checkCell(const ubyte& x, const ubyte& y, const Type& type, const Color& opponentColor)
{
	Piece*& cell = (*this)(x, y, false);
	return cell && cell->getType() == type && cell->getColor() == opponentColor;
}

const bool Board::isKingThreatened(const Piece& king)
{
	const Color opponentColor = (Color)!(bool)king.getColor();
	const ubyte& kingX = king.getX();
	const ubyte& kingY = king.getY();
	/* ####### Checking In Straight Lines ####### */

	// horizontally left
	for (ubyte i = kingX - 1; i != MAX_UBYTE; i--) // ubyte is unsinged to check x to 0 (inclusive)
	{
		Piece*& current = (*this)(i, kingY, false);
		if (current)
		{
			// Found a possible hostile piece
			const Type& t = current->getType(); // Rename
			if ((t == ROOK || t == QUEEN) && opponentColor == current->getColor()) return true;
			break; // Found a non-hostile piece
		}
	}

	// horizontally right
	for (ubyte i = kingX + 1; i < COLS; i++)
	{
		Piece*& current = (*this)(i, kingY, false);
		if (current)
		{
			const Type& t = current->getType(); // Rename
			if ((t == ROOK || t == QUEEN) && opponentColor == current->getColor()) return true;
			break;
		}
	}

	// vertically up
	for (ubyte i = kingY - 1; i != MAX_UBYTE; i--)
	{
		Piece*& current = (*this)(kingX, i, false);
		if (current)
		{
			const Type& t = current->getType(); // Rename
			if ((t == ROOK || t == QUEEN) && opponentColor == current->getColor()) return true;
			break;
		}
	}

	// vertically down
	for (ubyte i = kingY + 1; i < ROWS; i++)
	{
		Piece*& current = (*this)(kingX, i, false);
		if (current)
		{
			const Type& t = current->getType(); // Rename
			if ((t == ROOK || t == QUEEN) && opponentColor == current->getColor()) return true;
			break;
		}
	}

	/* ####### Checking Diagonals ####### */

	// right-up direction
	for (ubyte i = kingX + 1, j = kingY - 1; i < COLS && j != MAX_UBYTE; i++, j--)
	{
		Piece*& current = (*this)(i, j, false);
		if (current)
		{
			const Type& t = current->getType(); // Rename
			if ((t == BISHOP || t == QUEEN) && opponentColor == current->getColor()) return true;
			break;
		}
	}

	// right-down direction
	for (ubyte i = kingX + 1, j = kingY + 1; i < COLS && j < ROWS; i++, j++)
	{
		Piece*& current = (*this)(i, j, false);
		if (current)
		{
			const Type& t = current->getType(); // Rename
			if ((t == BISHOP || t == QUEEN) && opponentColor == current->getColor()) return true;
			break;
		}
	}

	// left-up direction
	for (ubyte i = kingX - 1, j = kingY - 1; i != MAX_UBYTE && j != MAX_UBYTE; i--, j--)
	{
		Piece*& current = (*this)(i, j, false);
		if (current)
		{
			const Type& t = current->getType(); // Rename
			if ((t == BISHOP || t == QUEEN) && opponentColor == current->getColor()) return true;
			break;
		}
	}

	// left-down direction
	for (ubyte i = kingX - 1, j = kingY + 1; i != MAX_UBYTE && j < ROWS; i--, j++)
	{
		Piece*& current = (*this)(i, j, false);
		if (current)
		{
			const Type& t = current->getType(); // Rename
			if ((t == BISHOP || t == QUEEN) && opponentColor == current->getColor()) return true;
			break;
		}
	}

	/* ####### Checking Pawn Threats ####### */

	if (opponentColor == Black)
	{
		if (checkCell(kingX + 1, kingY - 1, PAWN, opponentColor)) return true;
		if (checkCell(kingX - 1, kingY - 1, PAWN, opponentColor)) return true;
	}
	else
	{
		if (checkCell(kingX + 1, kingY + 1, PAWN, opponentColor)) return true;
		if (checkCell(kingX - 1, kingY + 1, PAWN, opponentColor)) return true;
	}

	/* ####### Checking Knight Threats ####### */

	if (checkCell(kingX + 1, kingY - 2, KNIGHT, opponentColor)) return true;
	if (checkCell(kingX + 2, kingY - 1, KNIGHT, opponentColor)) return true;
	if (checkCell(kingX + 2, kingY + 1, KNIGHT, opponentColor)) return true;
	if (checkCell(kingX + 1, kingY + 2, KNIGHT, opponentColor)) return true;
	if (checkCell(kingX - 1, kingY + 2, KNIGHT, opponentColor)) return true;
	if (checkCell(kingX - 2, kingY + 1, KNIGHT, opponentColor)) return true;
	if (checkCell(kingX - 2, kingY - 1, KNIGHT, opponentColor)) return true;
	if (checkCell(kingX - 1, kingY - 2, KNIGHT, opponentColor)) return true;

	/* ####### Checking Naughty King ####### */

	if (checkCell(kingX, kingY - 1, KING, opponentColor)) return true;
	if (checkCell(kingX, kingY + 1, KING, opponentColor)) return true;
	if (checkCell(kingX + 1, kingY, KING, opponentColor)) return true;
	if (checkCell(kingX - 1, kingY, KING, opponentColor)) return true;
	if (checkCell(kingX - 1, kingY - 1, KING, opponentColor)) return true;
	if (checkCell(kingX + 1, kingY - 1, KING, opponentColor)) return true;
	if (checkCell(kingX - 1, kingY + 1, KING, opponentColor)) return true;
	if (checkCell(kingX + 1, kingY + 1, KING, opponentColor)) return true;

	return false;
}

const int Board::makeMove(const std::string& moveStr)
{
	ErrCode error = validMove;

	// should handle the string sent from graphics
	// according the protocol. Ex: e2e4           (move e2 to e4)
	const coords* move = Utils::parseFrontendString(moveStr);
	try
	{
		Piece*& srcPiece = (*this)(move[0].x, move[0].y);
		if (srcPiece)
			(*this)(move[0].x, move[0].y)->move(move[1].x, move[1].y);
		else
			throw invalidMove_SrcCellDoesNotContainFriendlyPiece;
	}
	catch (const ErrCode& e)
	{
		error = e;
		std::cout << "Error " << e << '\n';
	}

	if (Utils::isValid(error)) /* if a valid move was made*/ this->updateTurn();
	delete move;

	return error;
}
