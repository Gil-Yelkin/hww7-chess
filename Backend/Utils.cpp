#include "Utils.h"

const coords* Utils::parseFrontendString(const std::string& str)
{
	coords src{}, dst{};
	// Normalizing ASCII chars to their respective cell indexes on board
	src.x = str[0] - 'a';
	src.y = 8 - str[1] + '0';
	dst.x = str[2] - 'a';
	dst.y = 8 - str[3] + '0';

	coords* move = new coords[2];
	move[0] = src;
	move[1] = dst;
	return move;
}

const bool Utils::isValid(const ErrCode& e)
{
	return e == validMove || e == validMove_Check || e == validMove_CheckMate;
}

const ubyte Utils::uabs(const ubyte& num)
{
	if (num & 0b10000000) /* checking if MSB is set */
		return (ubyte)-num;

	return num;
}
