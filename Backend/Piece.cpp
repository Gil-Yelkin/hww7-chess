#include "Piece.h"

Piece* Piece::nullPiece = nullptr;

Piece::Piece(const ubyte& x, const ubyte& y, const Color& color, Board& board)
    : x(x), y(y), color(color), board(board)
{
    // params are assumed to be valid, given by board.cpp
}

const Color& Piece::getColor() const
{
    return this->color;
}

const ubyte Piece::getX() const
{
    return this->x;
}

const ubyte Piece::getY() const
{
    return this->y;
}

void Piece::move(const ubyte& x, const ubyte& y)
{
    this->validateMove(x, y);

    // moving the piece if the move is valid
    const ubyte oldX = this->x;
    const ubyte oldY = this->y;

    Piece* oldPiece = board(x, y);
    this->board(this->x, this->y) = nullptr;
    this->x = x;
    this->y = y;
    this->board(x, y) = this;

    //checking if the move created a check and reversing the move if so
    if (this->board.isKingThreatened(this->board.getTurn() == White ? this->board.whiteKing : this->board.blackKing))
    {
        this->board(this->x, this->y) = oldPiece;
        this->x = oldX;
        this->y = oldY;
        this->board(this->x, this->y) = this;
        throw invalidMove_SelfCheckWouldBeCreated;
    }
    else if (this->board.isKingThreatened(this->board.getTurn() == Black ? this->board.whiteKing : this->board.blackKing))
    {    
        throw validMove_Check;
    }
}

void Piece::validateMove(const ubyte& x, const ubyte& y) const
{
    // Index out-of-bounds is checked in board () operator
    if (this->x == x && this->y == y)
        throw invalidMove_SrcAndDstAreEqual;
    if (this->color != this->board.getTurn())
        throw invalidMove_SrcCellDoesNotContainFriendlyPiece;
    if (this->board(x, y) && this->color == this->board(x, y)->color)
        throw invalidMove_DstCellContainsFriendlyPiece;
}
