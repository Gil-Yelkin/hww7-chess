#pragma once
#include <string>
#include "Piece.h"

class Piece;

class Board
{
private:
	Piece* pieces[COLS][ROWS];
	Color turn;

public:
	Board();
	~Board();

	Piece& blackKing;
	Piece& whiteKing;
	
	// defining operator() to simulate double bracket accessing (board[][]) which is impossible to implement without a helper class
	Piece*& operator()(const ubyte& x, const ubyte& y, const bool& doThrow = true);
	Piece*& operator()(const coords& pos, const bool& doThrow = true);

	const bool checkCell(const ubyte& x, const ubyte& y, const Type& type, const Color& opponentColor);
	const bool isKingThreatened(const Piece& king);
	const int makeMove(const std::string& moveStr);

	// getters
	const Color getTurn() const;
	void updateTurn();
};
