#include "King.h"

King::King(const ubyte& x, const ubyte& y, const Color& color, Board& board)
	: Piece(x, y, color, board)
{}

const Type King::getType() const { return KING; }

void King::validateMove(const ubyte& x, const ubyte& y) const
{
	Piece::validateMove(x, y);

	// Check if moving beyond king's reach
	const ubyte dx = Utils::uabs(this->x - x);
	const ubyte dy = Utils::uabs(this->y - y);
	if (dx > 1 || dy > 1)
		throw invalidMove_IllegalMove;

	// this->hasMoved = true;
}