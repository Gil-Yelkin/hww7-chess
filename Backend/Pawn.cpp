#include "Pawn.h"

Pawn::Pawn(const ubyte& x, const ubyte& y, const Color& color, Board& board)
	: Piece(x, y, color, board), hasMoved(false)
{}

const Type Pawn::getType() const { return PAWN; }

void Pawn::validateMove(const ubyte & x, const ubyte & y) const
{
	Piece::validateMove(x, y);

	if (x == this->x)
	{
		if (this->board(x, y, false)) throw invalidMove_IllegalMove;

		if (!hasMoved)		
			if (y == (this->y + (this->color == White ? -2 : 2))) return;		
	}

	if (y != (this->y + (this->color == White ?  - 1 : 1)))
		throw invalidMove_IllegalMove;
	else
	{
		if (x == this->x + 1 || x == this->x - 1)
		{
			if (!this->board(x, y, false)) throw invalidMove_IllegalMove;
		}
		else if (x != this->x)
		{
			throw invalidMove_IllegalMove;
		}
	}
}
