#pragma once
#include "Piece.h"

class Knight : public Piece
{
public:
	Knight(const ubyte& x, const ubyte& y, const Color& color, Board& board);
	virtual const Type getType() const override;
	virtual void validateMove(const ubyte& x, const ubyte& y) const override;
};
