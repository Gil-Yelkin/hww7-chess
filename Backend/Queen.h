#pragma once
#include "Piece.h"
#include "Rook.h"
#include "Bishop.h"

class Queen : public Piece
{
public:
	Queen(const ubyte& x, const ubyte& y, const Color& color, Board& board);
	virtual const Type getType() const override;
	virtual void validateMove(const ubyte& x, const ubyte& y) const override;
};
