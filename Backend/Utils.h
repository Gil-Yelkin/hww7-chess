#pragma once
#include <string>

typedef unsigned char ubyte;
#define MAX_UBYTE (ubyte)(-1)
#define INITIAL_BOARD_STR "rnbqkbnrpppppppp################################PPPPPPPPRNBQKBNR0" // this string represents the initial state of the board

// Board defines
#define ROWS 8
#define COLS 8
#define NUM_CELLS_IN_BOARD ROWS * COLS
enum Color {White, Black};

enum Type
{
	ROOK,
	BISHOP,
	KNIGHT,
	QUEEN,
	KING,
	PAWN
};


enum ErrCode
{
	validMove,
	validMove_Check,
	invalidMove_SrcCellDoesNotContainFriendlyPiece,
	invalidMove_DstCellContainsFriendlyPiece,
	invalidMove_SelfCheckWouldBeCreated,
	invalidMove_InvalidCellIndex,
	invalidMove_IllegalMove,
	invalidMove_SrcAndDstAreEqual,
	validMove_CheckMate
};

struct coords
{
	ubyte x, y;
};


class Utils
{
public:
	static const ubyte uabs(const ubyte& num); // same as built-in abs() but for unsigned byte
	static const coords* parseFrontendString(const std::string& str);
	static const bool isValid(const ErrCode& e);
};
