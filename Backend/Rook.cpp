#include "Rook.h"

Rook::Rook(const ubyte& x, const ubyte& y, const Color& color, Board& board)
	: Piece(x, y, color, board)
{}

const Type Rook::getType() const { return ROOK; }

void Rook::validateMove(const ubyte& x, const ubyte& y) const
{
	Piece::validateMove(x, y);

	// Check if going in a gay line
	if (x != this->x && y != this->y)
		throw invalidMove_IllegalMove;
	
	validateRook(this->x, this->y, x, y, this->board);
}

void Rook::validateRook(ubyte xCurr, ubyte yCurr, const ubyte& xDst, const ubyte& yDst, Board& board)
{
	// Check if path is not clear
	ubyte axis, dst; // Rename x or y to general terms
	const bool movingX = xDst != xCurr;

	if (movingX)
	{
		axis = xCurr;
		dst = xDst;
	}
	else
	{
		axis = yCurr;
		dst = yDst;
	}

	const bool direction = (xDst > xCurr || yDst > yCurr);

	// Exclude current cell, then update axis according to direction
	for (/*hey shonny XD*/; direction ? (++axis < dst) : (--axis > dst);)
	{
		// Check if current cell is none
		if (movingX ? board(axis, yDst) : board(xDst, axis))
			throw invalidMove_IllegalMove;
	}
}
