#include "Queen.h"

Queen::Queen(const ubyte& x, const ubyte& y, const Color& color, Board& board)
	: Piece(x, y, color, board)
{}

const Type Queen::getType() const { return QUEEN; }

void Queen::validateMove(const ubyte & x, const ubyte & y) const
{
	Piece::validateMove(x, y);

	// Handle cases where going in a Rook pattern, a Bishop pattern and an invalid pattern
	if (!(x != this->x && y != this->y))
		Rook::validateRook(this->x, this->y, x, y, this->board);
	else if (abs(x - this->x) == abs(y - this->y))
		Bishop::validateBishop(this->x, this->y, x, y, this->board);
	else
		throw invalidMove_IllegalMove;
}
