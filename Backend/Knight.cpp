#include "Knight.h"

Knight::Knight(const ubyte& x, const ubyte& y, const Color& color, Board& board)
	: Piece(x, y, color, board)
{}

const Type Knight::getType() const { return KNIGHT; }

void Knight::validateMove(const ubyte & x, const ubyte & y) const
{
	Piece::validateMove(x, y);
	// checking if trying to move beyond knight's reach
	if (Utils::uabs(this->x - x) * Utils::uabs(this->y - y) != 2)
		throw invalidMove_IllegalMove;
}
